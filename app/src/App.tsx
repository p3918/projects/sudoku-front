import React from 'react';
import './App.scss';
import {useAppState} from "./Navigation/AppState";
import Header from "./View/Pages/Header";
import GridResolverPage from "./View/Pages/GridResolverPage";
import TechniquesPage from "./View/Pages/TechniquesPage";
import TutorialPage from "./View/Pages/TutorialPage";

function App() {
  const appState = useAppState();
  const debug = process.env.NODE_ENV === 'development';

  return (
    <div id="app-container">
      <Header {...appState} />
      <div className="app-content">
        {appState.menu === 'resolver' && <GridResolverPage debug={debug}/>}
        {appState.menu === 'techniques' && <TechniquesPage/>}
        {appState.menu === 'tutorial' && <TutorialPage/>}
      </div>
    </div>
  );
}

export default App;
