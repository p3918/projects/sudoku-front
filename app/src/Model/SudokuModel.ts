export type CellModel = {
  value: null | number,
  effect: null | number,
  annotation: string,
  kumiKata: boolean[],
  lockedKata: boolean[]
}

export type GridModel = CellModel[]

export type StepModel = {
  title: string,
  level: number,
  kumiKata1: boolean,
  kumiKata2: boolean,
  proof: string[],
  messages: string[],
  grid1: GridModel,
  grid2: GridModel
}

export const emptyCell: () => CellModel = () => {
  return {
    value: null,
    effect: null,
    annotation: '',
    kumiKata: [true, true, true, true, true, true, true, true, true,],
    lockedKata: [false, false, false, false, false, false, false, false, false,]
  }
}

export const emptyGrid: () => GridModel = () => {
  return [
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
    emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(), emptyCell(),
  ]
}