import React from 'react';
import Step from "./Step";
import {StepModel} from "../../Model/SudokuModel";

const StepList: (props: { steps: StepModel[], debug: boolean }) => JSX.Element = (props) => {

  return <>
    {props.steps.map((step, index) => <Step key={index} index={index} {...step} deployed={false} debug={props.debug}/>)}
  </>
}
export default StepList;