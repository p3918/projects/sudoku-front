import * as React from 'react';
import {cellClass} from "../Helpers/CellClass";
import {CellModel} from "../../Model/SudokuModel";
import MiniCaseStatic from "./MiniCaseStatic";
import {cellPosition} from "../Helpers/CellPosition";

type StaticCellProps = {
  index: number,
  needKata: boolean,
  effects: boolean
} & CellModel

const StaticCell: (props: StaticCellProps) => JSX.Element = (props) => {

  const classN = cellClass({
    index: props.index, value: props.value, effect: props.effects ? props.effect : null, editMode: false,
  });
  const cellPositionInfos = cellPosition(props.index)

  if (props.value === null) {
    return <div className={classN} title={cellPositionInfos}>
      <div className="kumi-kata">
        {props.needKata && props.kumiKata.map(
          (enabled, value) =>
            <MiniCaseStatic key={value} value={value}
                            enabled={enabled}
                            title={"Le Kumi-Kata représente l'ensemble des valeurs possibles dans une case donnée.\nil doit être rempli entièrement en une seule fois"
                            }
            />
        )}
      </div>
      <div className="locked-horizontal">
        {props.lockedKata.map(
          (enabled, value) =>
            <MiniCaseStatic key={value} value={value}
                            enabled={enabled}
                            title={
                              "Les verrous représentent les seules positions possibles pour une valeur donnée dans le bloc concerné.\nOn les note au fur à mesure des projections, lorsqu'ils sont alignés uniquement."
                            }
            />
        )}
        {props.effects && (
          <div className="annotation">
            {props.annotation}
          </div>
        )}
      </div>

    </div>;
  }

  return <div className={classN} title={cellPositionInfos}>
    {(props.value + 1)}
    {props.effects && (
      <div className="annotation">
        {props.annotation}
      </div>
    )}
  </div>;

}

export default StaticCell;