import React from 'react';
import Step from "./Step";
import {StepModel} from "../../Model/SudokuModel";
import StepsGroup from "./StepsGroup";

type TechGroup = {
  title: string,
  steps: StepModel[]
}
const StepByTechniqueList: (props: { steps: StepModel[], debug: boolean }) => JSX.Element = (props) => {


  if (props.steps.length === 0) {
    return <></>;
  }

  let first = props.steps[0].title;
  let byTechnique: TechGroup[] = [{
    title: first,
    steps: []
  }];
  let currentIndex = 0;

  for (let i = 0; i < props.steps.length; i++) {
    if (props.steps[i].title === byTechnique[currentIndex].title) {
      byTechnique[currentIndex].steps.push(props.steps[i]);
    } else {
      byTechnique.push({
        title: props.steps[i].title,
        steps: [props.steps[i]]
      });
      currentIndex++;
    }
  }

  return <>
    {byTechnique.map((stepGroup, index) => {
      return stepGroup.steps.length > 1 ? <StepsGroup {...stepGroup} key={index} debug={props.debug}/> :
        <Step {...stepGroup.steps[0]} index={index} key={index} deployed={false} debug={props.debug}/>
    })
    }
  </>
}
export default StepByTechniqueList;