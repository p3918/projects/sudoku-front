import React from 'react';
import Step from "./Step";
import {StepModel} from "../../Model/SudokuModel";
import StepsGroup from "./StepsGroup";

type TechGroup = {
  title: string,
  level: number,
  steps: StepModel[]
}
const StepByLevelList: (props: { steps: StepModel[], expandFrom?: number, debug: boolean }) => JSX.Element = (props) => {

  const expandFrom = props.expandFrom || 2;
  const title: (l: number) => string = (l) => {
    switch (l) {
      case 0:
        return 'Basique';
      case 1:
        return 'Avancé';
      case 2:
        return 'Expert';
      default:
        return 'Très difficile';
    }
  }
  if (props.steps.length === 0) {
    return <></>;
  }

  let byLevel: TechGroup[] = [{
    title: title(props.steps[0].level),
    level: props.steps[0].level,
    steps: []
  }];
  let currentIndex = 0;

  for (let i = 0; i < props.steps.length; i++) {
    if (
      byLevel[currentIndex].steps.length === 0 ||
      (props.steps[i].level === byLevel[currentIndex].level && byLevel[currentIndex].level < expandFrom)
    ) {
      byLevel[currentIndex].steps.push(props.steps[i]);
    } else {
      byLevel.push({
        title: title(props.steps[i].level),
        level: props.steps[i].level,
        steps: [props.steps[i]]
      });
      currentIndex++;
    }
  }

  return <>
    {byLevel.map((stepGroup, index) => {
      return stepGroup.steps.length > 1 ? <StepsGroup {...stepGroup} key={index} debug={props.debug}/> :
        <Step {...stepGroup.steps[0]}
              title={stepGroup.title + " (" + stepGroup.steps[0].title + ")"}
              index={index} key={index} deployed={false}
              debug={props.debug}
        />
    })
    }
  </>
}
export default StepByLevelList;