import React, {useContext, useState} from 'react';
import {StepModel} from "../../Model/SudokuModel";
import StaticGrid from "./StaticGrid";
import Message from "../Helpers/Message";
import {reportBug} from "../../Navigation/Api";
import {InitialGrid} from "../Helpers/InitialGrid";

const Step: (props: StepModel & { deployed: boolean, index: number, debug: boolean }) => JSX.Element = (props) => {

  const [deployed, setDeployed] = useState<boolean>(props.deployed)
  const [detailed, setDetailed] = useState<boolean>(false)
  const [forceKata, setForceKata] = useState<boolean>(false)
  const [bugReport, setBugReport] = useState<string>('')
  const [reportLoading, setReportLoading] = useState<boolean>(false)
  const [reportSent, setReportSent] = useState<boolean>(false)
  const gridI = useContext(InitialGrid);

  const sendReport = () => {
    setReportLoading(true);
    reportBug({
      message: bugReport,
      gridI: gridI,
      grid1: props.grid1,
      grid2: props.grid2
    }).then(response => {
      setReportLoading(false)
      setReportSent(true)
      setBugReport('')
    });
  }

  if (deployed) {
    return (
      <div className="grid-stepper">
        <div className="title-container" onClick={() => setDeployed(false)}>
          <span className="deploy">-</span> <h3>{props.title || 'NA'}</h3>
        </div>
        <div className="grids-container">
          <StaticGrid grid={props.grid1} kumiKata={forceKata || props.kumiKata1} effects={false}
                      effectsFrom={props.grid2}/>
          <StaticGrid grid={props.grid2} kumiKata={forceKata || props.kumiKata2} effects={true}/>
        </div>
        <div className="content-container">
          <h5>Actions</h5>
          <p>{props.messages.map((m, i) => <Message key={i} message={m}/>)}</p>
          {detailed ? (
            <>
              <div className="title-container" onClick={() => setDetailed(false)}>
                <span className="deploy deploy-small">-</span> <h5>Explications</h5>
              </div>
              <p>{props.proof.map((m, i) => <Message key={i} message={m}/>)}</p>
              <br/>
              {props.debug &&
              <>{!props.kumiKata1 && <button onClick={() => setForceKata(!forceKata)}>Force KATA</button>}
                  <button onClick={() => console.log('grid1', props.grid1)}>Debug Grid 1</button>
                  <button onClick={() => console.log('grid2', props.grid2)}>Debug Grid 2</button>
                  <br/>
              </>
              }
              {
                reportLoading ? "Envoi en cours ..." : <>
                  <input type="text" size={100}
                         placeholder={reportSent ? "Rapport envoyé, merci de votre contribution" : "Un souci à cette étape ? Décrivez le problème (incompréhension, erreur, etc.) et cliquez sur Signaler"}
                         title="Les 2 grilles ci-dessous seront automatiquement envoyées avec votre message"
                         value={bugReport} onChange={(e) => setBugReport(e.target.value)}
                  />
                  {!reportSent && <>&nbsp;&nbsp;
                      <button onClick={sendReport} disabled={reportSent}>Signaler</button>
                  </>}
                </>
              }
            </>
          ) : (
            <div className="title-container" onClick={() => setDetailed(true)}>
              <span className="deploy primary" title={"Voir plus d'explications / Signaler un problème"}>?</span>
              <h3>En savoir plus</h3>
            </div>
          )}
        </div>
      </div>
    )
  }
  return <div className="grid-stepper">
    <div className="title-container" onClick={() => setDeployed(true)}>
      <span className="deploy">+</span> <h3>{props.title || 'NA'}</h3>
    </div>
  </div>

}

export default Step;