import * as React from 'react';
import {ChangeEvent, MutableRefObject, useEffect, useRef} from 'react';
import {cellClass} from "../Helpers/CellClass";
import {CellModel} from "../../Model/SudokuModel";
import KeyCodes from "../Helpers/KeyCodes";

type EditorCellProps = {
  index: number,
  setValue: (v: null | number) => void,
  enabled: boolean,
  focus: boolean,
  onClick: () => void
} & CellModel

const EditorCell: (props: EditorCellProps) => JSX.Element = (props) => {

  const classN = cellClass({
    index: props.index, value: props.value, effect: props.effect, editMode: true
  });

  const useFocus: () => [MutableRefObject<any>, () => void] = () => {
    const inputRef: MutableRefObject<any> = useRef(null)
    const setFocus: () => void = () => {
      if (inputRef && inputRef.current) {
        inputRef.current.focus()
      }
    }

    return [inputRef, setFocus]
  }

  const [inputRef, setInputFocus] = useFocus()
  useEffect(() => {
    if (props.focus) {
      setInputFocus()
    }
  }, [props.focus])

  const preventInputDefaultBehaviour = (e: any) => {
    if (e.keyCode === KeyCodes.KEY_ENTER
      || e.keyCode === KeyCodes.KEY_LEFT
      || e.keyCode === KeyCodes.KEY_RIGHT
      || e.keyCode === KeyCodes.KEY_TAB
      || e.keyCode === KeyCodes.KEY_UP
      || e.keyCode === KeyCodes.KEY_DOWN
    ) {
      e.preventDefault();
    }
  }

  const strValue = (props.value === null) ? "" : ((props.value + 1) + "")
  const setValue = props.enabled ? (e: ChangeEvent<HTMLInputElement>) => {
    props.setValue(e.currentTarget.value.length > 0 ? (Number.parseInt(e.currentTarget.value) - 1) : null)
  } : () => {
  };
  return (
    <div className={classN}>
      <input autoFocus={props.index === 0} type="number" value={strValue}
             onChange={setValue} disabled={!props.enabled}
             onKeyUp={preventInputDefaultBehaviour} onKeyDown={preventInputDefaultBehaviour}
             onKeyPress={preventInputDefaultBehaviour}
             ref={inputRef}
             onClick={props.onClick}
      />
    </div>)
    ;

}

export default EditorCell;