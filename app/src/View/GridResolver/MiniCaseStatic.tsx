import * as React from 'react';

type MiniCaseProps = {
  value: number,
  enabled: boolean,
  title: string
}
const MiniCaseStatic: (props: MiniCaseProps) => JSX.Element = (props) => {

  return <div className="mini-case center" title={props.title}
  >
    {props.enabled && (props.value + 1)}
  </div>
}

export default MiniCaseStatic;