import * as React from 'react';
import {useState} from 'react';
import {GridModel} from "../../Model/SudokuModel";
import EditorCell from "./EditorCell";
import {useKeyBoardSubscriber} from "../Helpers/KeyBoardManager";
import KeyCodes, {checkKeyMap, checkMeta} from "../Helpers/KeyCodes";

type LoaderEditorGridProps = {
  grid: GridModel,
  setValue: (i: number) => (v: null | number) => void,
  enabled: boolean
}

const LoaderEditorGrid: (props: LoaderEditorGridProps) => JSX.Element = (props) => {

  const [currentCellFocus, setCurrentCellFocus] = useState<number>(0)

  useKeyBoardSubscriber("key-listener",
    (e) => {
      e.preventDefault();
      if (e.keyCode === KeyCodes.KEY_ENTER
        || e.keyCode === KeyCodes.KEY_RIGHT
        || (e.keyCode === KeyCodes.KEY_TAB && !checkMeta(e, [KeyCodes.KEY_SHIFT]))
      ) {
        setCurrentCellFocus((currentCellFocus + 1) % 81)
      }
      if (e.keyCode === KeyCodes.KEY_LEFT
        || checkKeyMap(e, KeyCodes.KEY_TAB, [KeyCodes.KEY_SHIFT])
      ) {
        setCurrentCellFocus((currentCellFocus + 80) % 81)
      }
      if (e.keyCode === KeyCodes.KEY_UP) {
        setCurrentCellFocus((currentCellFocus + 72) % 81)
      }
      if (e.keyCode === KeyCodes.KEY_DOWN) {
        setCurrentCellFocus((currentCellFocus + 9) % 81)
      }
    }, [currentCellFocus]);

  return (
    <div className="grid-wrapper">
      <div className="grid">
        {props.grid.map(
          (cell, index) =>
            <EditorCell key={index} index={index}
                        focus={index === currentCellFocus}
                        {...cell}
                        setValue={props.setValue(index)} enabled={props.enabled}
                        onClick={() => setCurrentCellFocus(index)}
            />
        )
        }
      </div>

      <div/>

      <div className="center">C1</div>
      <div className="center">C2</div>
      <div className="center">C3</div>
      <div className="center">C4</div>
      <div className="center">C5</div>
      <div className="center">C6</div>
      <div className="center">C7</div>
      <div className="center">C8</div>
      <div className="center">C9</div>

      <div className="center">L1</div>
      <div className="center">L2</div>
      <div className="center">L3</div>
      <div className="center">L4</div>
      <div className="center">L5</div>
      <div className="center">L6</div>
      <div className="center">L7</div>
      <div className="center">L8</div>
      <div className="center">L9</div>
    </div>
  )

}

export default LoaderEditorGrid;