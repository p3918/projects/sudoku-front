import React, {useState} from 'react';
import {StepModel} from "../../Model/SudokuModel";
import StaticGrid from "./StaticGrid";
import StepList from "./StepList";

const StepsGroup: (props: { steps: StepModel[], title?: string, debug: boolean }) => JSX.Element = (props) => {

  const [deployed, setDeployed] = useState<boolean>(false)
  const [detailed, setDetailed] = useState<boolean>(false)

  if (props.steps.length === 0) {
    return <></>
  }
  const firstGrid = props.steps[0].grid1;
  const lastGrid = props.steps[props.steps.length - 1].grid2;

  const globalStep: StepModel = {
    grid1: firstGrid,
    grid2: lastGrid,
    kumiKata1: props.steps[0].kumiKata1,
    kumiKata2: props.steps[props.steps.length - 1].kumiKata2,
    level: props.steps[props.steps.length - 1].level,
    messages: [],
    proof: [],
    title: props.title || props.steps[props.steps.length - 1].title
  }

  if (deployed) {
    return (
      <div className="grid-stepper">
        <div className="title-container" onClick={() => setDeployed(false)}>
          <span className="deploy">-</span> <h3>{globalStep.title || 'NA'}</h3>
        </div>
        <div className="grids-container">
          <StaticGrid grid={globalStep.grid1} kumiKata={globalStep.kumiKata1} effects={false}/>
          <StaticGrid grid={globalStep.grid2} kumiKata={globalStep.kumiKata2} effects={true}/>
        </div>
        <div className="content-container">
          <div className="title-container" onClick={() => setDetailed(!detailed)}>
            <span className="deploy">{detailed ? '-' : '?'}</span> <h3>Étapes</h3>
          </div>
          {detailed &&
          <StepList steps={props.steps} debug={props.debug}/>
          }
        </div>
      </div>
    )
  }
  return <div className="grid-stepper">
    <div className="title-container" onClick={() => setDeployed(true)}>
      <span className="deploy">+</span> <h3>{globalStep.title || 'NA'}</h3>
    </div>
  </div>
}
export default StepsGroup;