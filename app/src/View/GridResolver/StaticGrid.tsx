import * as React from 'react';
import StaticCell from "./StaticCell";
import {CellModel, GridModel} from "../../Model/SudokuModel";

type StaticGridProps = {
  kumiKata: boolean,
  grid: GridModel,
  effects: boolean,
  effectsFrom?: GridModel
}
const StaticGrid: (props: StaticGridProps) => JSX.Element = (props) => {

  return (
    <div className="grid-wrapper">
      <div className="grid">
        {props.grid.map(
          (cell, index) => {
            if (!props.effectsFrom) {
              return <StaticCell key={index} index={index} {...cell} needKata={props.kumiKata} effects={props.effects}/>
            }
            const ncell: CellModel = {
              annotation: props.effectsFrom[index].annotation,
              effect: props.effectsFrom[index].effect,
              kumiKata: cell.kumiKata,
              lockedKata: cell.lockedKata,
              value: cell.value
            }
            return <StaticCell key={index} index={index} {...ncell} needKata={props.kumiKata} effects={true}/>
          }
        )
        }
      </div>
      <div/>

      <div className="center">C1</div>
      <div className="center">C2</div>
      <div className="center">C3</div>
      <div className="center">C4</div>
      <div className="center">C5</div>
      <div className="center">C6</div>
      <div className="center">C7</div>
      <div className="center">C8</div>
      <div className="center">C9</div>

      <div className="center">L1</div>
      <div className="center">L2</div>
      <div className="center">L3</div>
      <div className="center">L4</div>
      <div className="center">L5</div>
      <div className="center">L6</div>
      <div className="center">L7</div>
      <div className="center">L8</div>
      <div className="center">L9</div>

    </div>
  )

}

export default StaticGrid;