import React from "react";
import {emptyGrid} from "../../Model/SudokuModel";

export const InitialGrid = React.createContext(emptyGrid());
