import React from 'react';


const Message: (props: { message: string }) => JSX.Element = (props) => {

  const parts = props.message.split('#')

  const components: () => JSX.Element[] = () => {
    let comp = [];
    for (let i = 0; i < parts.length; i++) {
      if (i % 2 === 0) {
        comp.push(<React.Fragment key={i}>{parts[i]}</React.Fragment>)
      } else {
        comp.push(<span key={i} className={"effect-" + parts[i].substr(0, 1)}>{parts[i].substr(1)}</span>)
      }
    }
    return comp;
  }

  return (<>{components()}<br/></>);
}

export default Message;