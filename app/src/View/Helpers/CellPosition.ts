export const cellPosition: (index: number) => string = (index) => {
  const rowId = Math.floor(index / 9);
  const columnId = index % 9;
  const inBlockId = 3 * (rowId % 3) + (columnId % 3)
  const blockId = (Math.floor(rowId / 3) * 3) + Math.floor(columnId / 3)
  const inBlockRow = rowId % 3;
  const inBlockColumn = columnId % 3;
  return 'Bloc : B' + (blockId + 1) + '\n'
    + 'Position dans le bloc : case N°' + (inBlockId + 1)
    + ' ; ligne ' + (inBlockRow + 1) + ' ; colonne ' + (inBlockColumn + 1) + '\n'
    + 'Position dans la grille : [L' + (rowId + 1) + ';C' + (columnId + 1) + ']'
}