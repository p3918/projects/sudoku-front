import _ from "lodash";

export type CellClassProps = {
  index: number,
  value: null | number,
  editMode: boolean,
  effect: null | number
}

export const cellClass: (props: CellClassProps) => string = (props) => {
  const line = Math.floor(props.index / 9)
  const column = props.index % 9;
  return _.filter([
    column % 3 === 2 ? "block-border-right" : false,
    column % 3 === 0 ? "block-border-left" : false,
    line % 3 === 0 ? "block-border-top" : false,
    line % 3 === 2 ? "block-border-bottom" : false,
    "case",
    (props.value === null && !props.editMode) ? "detailed" : "center",
    (props.value !== null || props.editMode) && "big",
    props.effect === 1 && "effect-1",
    props.effect === 2 && "effect-2",
    props.effect === 3 && "effect-3",
    props.effect === 4 && "effect-4",
    props.effect === 5 && "effect-5",
    props.effect === 6 && "effect-6",
    props.effect === 7 && "effect-7",
    props.effect === 8 && "effect-8",
  ]).join(' ')
}