import React, {useContext, useEffect, useState} from "react";
import _ from "lodash";

// noinspection JSUnusedLocalSymbols
const KeyBoardActionContext = React.createContext({
  actions: {},
  addAction: (index: string, action: any) => {
  },
  removeAction: (index: string) => {
  },
});

const useKeyBoardActionState = () => {

  const [actionsDictionary, setActionsDictionary] = useState({});

  const addAction = (index: string, action: any) => {
    setActionsDictionary(previous => {
      const newAction = [index, action];
      const previousPairs = _.toPairs(previous);
      const next = [newAction, ...previousPairs];
      return _.fromPairs(next);
    });
  };

  const removeAction = (index: string) => {
    setActionsDictionary(previous => {
      let next = _.assign({}, previous);
      _.unset(next, index);
      return next;
    })
  };

  const executeActions = (e: KeyboardEvent) => {
    _.mapValues(actionsDictionary, (action: any) => (action(e)));
  };

  const upKey = (e: KeyboardEvent) => {
    executeActions(e);
  }

  useEffect(() => {

    window.addEventListener("keyup", upKey);
    return () => {
      window.removeEventListener("keyup", upKey);
    };
  });

  return {
    actions: actionsDictionary,
    addAction: addAction,
    removeAction: removeAction
  }
}

const useKeyBoardContextAndState = () => {
  const keyboardState = useKeyBoardActionState();
  const keyboardContext = {
    actions: keyboardState.actions,
    addAction: keyboardState.addAction,
    removeAction: keyboardState.removeAction
  }
  return [keyboardState, keyboardContext];
}

const KeyBoardManager = (props: any) => {

  const [keyboardState, keyboardContext] = useKeyBoardContextAndState();

  return (
    <KeyBoardActionContext.Provider value={keyboardContext}>
      {props.children}
    </KeyBoardActionContext.Provider>
  );
}

const useKeyBoardSubscriber = (uid: string, action = (e: KeyboardEvent) => {
}, listenChanges: any[] = []) => {

  const keyboardContext = useContext(KeyBoardActionContext);

  useEffect(() => {
    if (uid) {
      keyboardContext.addAction(uid, action);
      return () => {
        keyboardContext.removeAction(uid);
      }
    } else {
      console.error(uid, "is undefined !");
    }
  }, [uid, ...listenChanges]);

  return [];
}

export {KeyBoardManager, useKeyBoardSubscriber};