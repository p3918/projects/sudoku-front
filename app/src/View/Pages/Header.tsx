import React from 'react';
import {AppState} from "../../Navigation/AppState";

const Header: (props: AppState) => JSX.Element = (props) => {
  return (
    <header className="header-brand">
      <h1 className="app-title">Résolveur de Sudoku</h1>
      <ul className="header-nav">
        {/*<li className={props.menu === "resolver" ? "active" : ""}
            onClick={() => props.setMenu("resolver")}
        >Résolveur
        </li>
        <li className={props.menu === "techniques" ? "active" : ""}
            onClick={() => props.setMenu("techniques")}
        >Techniques
        </li>
        <li className={props.menu === "tutorial" ? "active" : ""}
            onClick={() => props.setMenu("tutorial")}
        >Tutoriel
        </li>*/}
      </ul>
    </header>
  );
}

export default Header;