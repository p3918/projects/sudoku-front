import React, {useEffect, useState} from 'react';
import {getGrid, getGridsList, getTechList, saveGrid, solveGrid, solveGridWithTech} from "../../Navigation/Api";
import LoaderEditorGrid from "../GridResolver/LoaderEditorGrid";
import {CellModel, emptyGrid, GridModel, StepModel} from "../../Model/SudokuModel";
import {clone} from "lodash";
import StaticGrid from "../GridResolver/StaticGrid";
import StepList from "../GridResolver/StepList";
import StepByTechniqueList from "../GridResolver/StepByTechniqueList";
import StepByLevelList from "../GridResolver/StepByLevelList";
import useLocalStorage from "../Helpers/LocalStorage";
import {KeyBoardManager} from "../Helpers/KeyBoardManager";
import {InitialGrid} from "../Helpers/InitialGrid";

type GroupBy = 'step' | 'technique' | 'level';
const GridResolverPage: (props: { debug: boolean }) => JSX.Element = (props) => {

  const [gridName, setGridName] = useState<string>('empty')
  const [newGridName, setNewGridName] = useState<string>('')
  const [gridList, setGridList] = useState<string[]>([])
  const [grid, setGrid] = useState<GridModel>(emptyGrid())
  const [steps, setSteps] = useState<StepModel[]>([])
  const [solvedGrid, setSolvedGrid] = useState<GridModel>(emptyGrid())
  const [groupBy, setGroupBy] = useLocalStorage<GroupBy>('preferredView', 'step')
  const [loading, setLoading] = useState<boolean>(false);
  const [techniques, setTechniques] = useState<any>([]);
  const [techniqueUsed, setTechniqueUsed] = useState<string | undefined>(undefined);
  const gridSolved = React.createRef<HTMLElement>();
  const stepsRef = React.createRef<HTMLElement>();

  const reset = () => {
    setGridName('empty');
    setNewGridName('');
    setSteps([]);
    setSolvedGrid(emptyGrid());
    setGrid(emptyGrid());
    setLoading(false);
    loadGrids();
  }

  const setValue: (i: number) => (v: null | number) => void = (i) => (v) => {
    setGrid(grid => {
      const newGrid = clone(grid);
      newGrid[i].value = v;
      return newGrid;
    })
  }

  useEffect(() => {
    setSteps([]);
    setSolvedGrid(emptyGrid());
    setGrid(emptyGrid());
    setLoading(true);
    getGrid(gridName).then((response: GridModel) => {
      setGrid(response);
      setLoading(false);
    }).catch(error => {
      console.error(error)
      setGrid(emptyGrid())
      setLoading(false)
    })
  }, [gridName])

  const loadGrids = () => {
    getGridsList().then(response => setGridList(response))
      .catch(error => {
        console.error(error)
        setGridList([])
      })
    ;
  }
  useEffect(() => {
    loadGrids()
  }, []);

  useEffect(() => {
    getTechList().then(response => {
        console.log("techniques = ", response)
        const techIds = Object.keys(response);
        const techNames = Object.values(response)
        const techs = [];
        for (let i = 0; i < techIds.length; i++) {
          techs.push({
            id: techIds[i],
            name: techNames[i]
          });
        }
        console.log(techs);
        setTechniques(techs);
      }
    ).catch(error => {
      console.error(error)
      setTechniques([])
    });
  }, []);

  useEffect(() => {
    if (gridSolved.current) {
      gridSolved.current.scrollIntoView({behavior: 'smooth'})
    }
  });

  const solveTheGrid = () => {

    if (newGridName.length > 2 && props.debug) {
      saveGrid(grid, newGridName).then(response => console.log("saved", response))
        .catch(error => {
          console.error(error)
        })
      ;
    }
    console.log('solving...');
    setSteps([]);
    setSolvedGrid(emptyGrid());
    setLoading(true);
    const promise = (techniqueUsed && techniqueUsed.length > 2) ?
      solveGridWithTech(grid, techniqueUsed)
      : solveGrid(grid);
    promise.then(response => {
      const responseSteps = [];
      for (let i = 1; i < response.length; i++) {
        responseSteps.push({
          title: response[i].title,
          level: response[i].level,
          kumiKata1: response[i - 1].kumiKata,
          kumiKata2: response[i].kumiKata,
          proof: response[i].proof,
          messages: response[i].messages,
          grid1: response[i - 1].grid,
          grid2: response[i].grid
        })
      }
      setSteps(responseSteps);
      let solvedGrid: GridModel = JSON.parse(JSON.stringify(response[response.length - 1].grid));
      solvedGrid.map((cell: CellModel, index: number) => {
        // @ts-ignore: Object is possibly 'null'.
        cell.effect = (grid && grid[index] && grid[index].value !== null) ? 7 : 8;
        cell.annotation = "";
      })
      setSolvedGrid(solvedGrid);
      setLoading(false);
      console.log('solve end', responseSteps)
    }).catch(error => {
      console.error(error)
      setLoading(false)
    });
  }
  return (
    <>
      <div className="grid-stepper">
        <div className="content-container">
          <h3>Votre grille</h3><br/>
          <p>Entrez les chiffres de la grille initiale ci-dessous, puis cliquez sur résoudre pour voir la solution et
            les étapes de résolution.</p>
        </div>
        <div className="grids-container">
          {steps.length > 0 && <span id="grid-anchor" ref={gridSolved}/>}
          <KeyBoardManager>
            <LoaderEditorGrid grid={grid} setValue={setValue} enabled={!loading}/>
          </KeyBoardManager>
          {!loading && steps.length > 0 &&
          <StaticGrid kumiKata={false} grid={solvedGrid} effects={true}/>
          }
        </div>
        <div className="grids-container form-style">
          {loading ? '...' :
            <>
              {props.debug &&
              <>
                  <select value={gridName} onChange={(e) => {
                    setGridName(e.target.value)
                  }}>
                    {gridList.map((gn, key) => <option key={key} value={gn}>{gn}</option>)}
                  </select>
                  <input type="text" value={newGridName} onChange={(e) => setNewGridName(e.target.value)}
                         placeholder="sauvegarder"
                  />
              </>
              }
              <select value={techniqueUsed}
                      title={'Utiliser une seule technique'}
                      onChange={(e) => setTechniqueUsed(e.target.value)}
              >
                <option value={""}>Toutes les techniques</option>
                {techniques.map((technique: any, index: number | string) => <option value={technique.id}
                                                                                    key={index}>{technique.name}</option>)}
              </select>
              <button onClick={solveTheGrid} className={'btn-primary'}>Résoudre</button>
              <button onClick={reset} className={'btn-primary'}>Reset</button>
              <select value={groupBy}
                      title={'personnaliser l\'affichage des étapes'}
                      onChange={(e) => setGroupBy(e.target.value as GroupBy)}>
                <option value={'step'}>Vue détaillée, étape par étape</option>
                <option value={'technique'}>Vue groupée par technique</option>
                <option value={'level'}>Vue par niveau de difficulté</option>
              </select>
              {steps.length > 0 &&
              <div onClick={() => stepsRef.current && stepsRef.current.scrollIntoView({behavior: 'smooth'})}
                   className="deploy primary"
                   title={'Vous ne visualisez pas le guide de résolution ? Cliquez ici ou défilez vers le bas'}>
                  ↓
              </div>
              }
            </>
          }
        </div>
      </div>

      <InitialGrid.Provider value={grid}>
        {steps.length > 0 && <span id="steps-anchor" ref={stepsRef}/>}
        {groupBy === 'step' && <StepList steps={steps} debug={props.debug}/>}
        {groupBy === 'technique' && <StepByTechniqueList steps={steps} debug={props.debug}/>}
        {groupBy === 'level' && <StepByLevelList steps={steps} debug={props.debug}/>}
      </InitialGrid.Provider>
    </>
  )
}

export default GridResolverPage;