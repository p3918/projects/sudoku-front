import {useState} from 'react';

type EnumMenu = "resolver" | "techniques" | "tutorial"

export type AppState = {
  menu: EnumMenu,
  setMenu: (menu: EnumMenu) => void
}

export const useAppState: () => AppState = () => {
  const [menu, setMenu] = useState<EnumMenu>("resolver");
  return {menu, setMenu}
}