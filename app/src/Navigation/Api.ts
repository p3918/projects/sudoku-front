import axios from "axios";
import {GridModel} from "../Model/SudokuModel";

export const getGrid: (gridName: string) => Promise<GridModel> = (gridName: string) => {
  return axios.get("/api/grid/" + gridName).then(response => {
      console.log("after get : ", response.data);
      return response.data;
    }
  )
}

export const solveGrid: (grid: GridModel) => Promise<any[]> = (grid: GridModel) => {
  return axios.post("/api/resolve/full", grid).then(response => {
      console.log("after post : ", response.data);
      return response.data;
    }
  )
}

export const getTechList: () => Promise<any[]> = () => {
  return axios.get("/api/resolve/list").then(response => {
    console.log("after get : ", response.data);
    return response.data;
  })
}

export const solveGridWithTech: (grid: GridModel, technique: string) => Promise<any[]> = (grid: GridModel, technique: string) => {
  return axios.post("/api/resolve/" + technique, grid).then(response => {
      console.log("after post : ", response.data);
      return response.data;
    }
  )
}

export const saveGrid: (grid: GridModel, name: string) => Promise<any[]> = (grid: GridModel, name: string) => {
  return axios.post("/api/grid/" + name, grid).then(response => {
      console.log("after save : ", response.data);
      return response.data;
    }
  )
}

export const getGridsList: () => Promise<string[]> = () => {
  return axios.get("/api/grid/list").then(response => {
      console.log('list grids : ', response.data)
      return response.data;
    }
  )
}

export const reportBug: (content: any) => Promise<any[]> = (content: any) => {
  return axios.put("/api/report", content).then(response => {
      console.log("after post : ", response.data);
      return response.data;
    }
  )
}