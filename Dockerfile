FROM node:14 AS npm
COPY ./app/src /home/node/app/src
COPY ./app/public /home/node/app/public
COPY ./app/package.json /home/node/app/package.json
COPY ./app/package-lock.json /home/node/app/package-lock.json
COPY ./app/tsconfig.json /home/node/app/tsconfig.json
COPY ./app/tslint.json /home/node/app/tslint.json
WORKDIR /home/node/app
RUN npm install
RUN npm run build

FROM celinederoland/server-apache:1.0-rc AS apache-server
COPY --from=npm /home/node/app/build /var/www/html/public
WORKDIR /var/www/html/public
